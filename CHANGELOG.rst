=======
History
=======

Next Release
------------

0.11.0 (2021-10-17)
------------------
* By default, use the major tautomer for major microspecies calculations.
* Internal testing config changes.

0.10.0 (2021-10-06)
------------------
* Add and correct compound exceptions
* Refactor a number of models

0.4.0 (2021-07-15)
------------------
* Refactor the codebase to use domain-driven design (DDD)
