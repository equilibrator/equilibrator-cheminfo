# 13. Java Bridge

Date: 2021-06-17

## Status

Accepted

## Context

ChemAxon is software written in Java. Although a command line interface is
provided, it is very slow on startup and thus not very suitable for the hundreds
of tousands of molecules that we need to process. Several Python to Java bridges
exist and we need to make a choice.

## Decision

After testing [jnius](https://pyjnius.readthedocs.io/) and [JPype](https://jpype.readthedocs.io/), it has become clear that jnius is focused on Android support and that JPype is the more mature and well rounded project.

## Consequences

We use JPype with all the [pros and cons](https://jpype.readthedocs.io/en/latest/userguide.html#alternatives) that entails.
