# 12. Cheminformatics Backend

Date: 2021-06-17

## Status

Accepted

## Context

The main purpose of this package is to wrap existing cheminformatics software in
order to make it uniformly accessible to tasks in eQuilibrator. The main ones we
consider are the open source OpenBabel and RDKit frameworks. For additional
tasks, like dissociation constant prediction, we use the proprietary ChemAxon
software.

## Decision

RDKit by all public measures seems to be the more active project and has the
larger community. Thus we prefer it over OpenBabel. For now, we have to rely on
ChemAxon for some tasks.

## Consequences

We will not maintain the OpenBabel molecule wrapper beyond what already exists.
