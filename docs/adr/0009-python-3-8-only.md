# 9. Python 3.8+ Only

Date: 2021-06-17

## Status

Accepted

Supercedes [3. Python 3.6+ only](0003-python-3-6-only.md)

## Context

Python development is continuous and new language features become available.

## Decision

As Python 3.9 is already released and there is a release candidate for 3.10, it
seems reasonable to only support 3.8+.

## Consequences

Python 3.8 brings better type annotations and the Walruss operator (`:=`) which
should be used sparingly but can be very useful. As this is more of an internal
package, not supporting older versions, should not be an issue.
