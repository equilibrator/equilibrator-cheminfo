# 10. Python Package Versioning

Date: 2021-06-17

## Status

Accepted

Supercedes [4. Python package versioning](0004-python-package-versioning.md)

## Context

Versioneer requires a number of files to be maintained.

## Decision

We change from versioneer to versioneer-518 which leverages `pyproject.toml`
configuration such that the versioneer module itself is now a dependency instead
of a local package.

## Consequences

We need to modify some configuration files for black, tox, and CI pipelines.
