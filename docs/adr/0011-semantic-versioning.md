# 11. Semantic Versioning

Date: 2021-06-17

## Status

Accepted

## Context

There are many different versioning schemes out there.

## Decision

We adopt [semantic versioning](https://semver.org/) since it is a popular scheme and very reasonable.

## Consequences

We follow the guidelines for releases.
