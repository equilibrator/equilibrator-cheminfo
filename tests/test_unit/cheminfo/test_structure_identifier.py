# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Test the structure identifier value object."""


import pytest

from equilibrator_cheminfo.domain.model import StructureIdentifier


"XLYOFNOQVPJJNP-UHFFFAOYSA-O	InChI=1S/H2O/h1H2/p+1"


@pytest.mark.parametrize(
    "inchikey, inchi",
    [
        pytest.param("", "", marks=pytest.mark.raises(exception=AssertionError)),
        pytest.param(
            "some string",
            "InChI=1S/H2O/h1H2",
            marks=pytest.mark.raises(exception=AssertionError),
        ),
        pytest.param(
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "some string",
            marks=pytest.mark.raises(exception=AssertionError),
        ),
        (
            "XLYOFNOQVPJJNP-UHFFFAOYSA-O",
            "InChI=1S/H2O/h1H2",
        ),
    ],
)
def test_dunder_init(inchikey: str, inchi: str):
    """Test that the structure identifier is correctly initialized."""
    identifier = StructureIdentifier(inchikey=inchikey, inchi=inchi)
    assert identifier.inchikey == inchikey
    assert identifier.inchi == inchi


@pytest.mark.parametrize(
    "inchikey, inchi, expected",
    [
        (
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "InChI=1S/H2O/h1H2",
            "StructureIdentifier(InChIKey=XLYOFNOQVPJJNP-UHFFFAOYSA-N, InChI=1S/H2O/...)",  # noqa: E501
        ),
        (
            "MHYQSGUKJRWFDJ-MVSNXQKDSA-M",
            "InChI=1S/C69H124NO9P/c1-4-7-10-13-16-19-22-24-26-27-28-29-30-31-32-33-34-35-36-37-38-39-41-43-46-48-51-54-57-60-68(72)76-64-66(79-69(73)61-58-55-52-49-44-21-18-15-12-9-6-3)65-78-80(74,75)77-63-62-70-67(71)59-56-53-50-47-45-42-40-25-23-20-17-14-11-8-5-2/h16,19-20,23-24,26,28-29,31-32,34-35,66H,4-15,17-18,21-22,25,27,30,33,36-65H2,1-3H3,(H,70,71)(H,74,75)/p-1/b19-16-,23-20-,26-24-,29-28-,32-31-,35-34-/t66-/m1/s1",  # noqa: E501
            "StructureIdentifier(InChIKey=MHYQSGUKJRWFDJ-MVSNXQKDSA-M, InChI=1S/C69H124NO9P/...)",  # noqa: E501
        ),
    ],
)
def test_dunder_repr(inchikey: str, inchi: str, expected: str):
    """Test that the structure identifier is represented as expected."""
    assert repr(StructureIdentifier(inchikey=inchikey, inchi=inchi)) == expected


@pytest.mark.parametrize(
    "inchikey, inchi",
    [
        (
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "InChI=1S/H2O/h1H2",
        ),
        (
            "MHYQSGUKJRWFDJ-MVSNXQKDSA-M",
            "InChI=1S/C69H124NO9P/c1-4-7-10-13-16-19-22-24-26-27-28-29-30-31-32-33-34-35-36-37-38-39-41-43-46-48-51-54-57-60-68(72)76-64-66(79-69(73)61-58-55-52-49-44-21-18-15-12-9-6-3)65-78-80(74,75)77-63-62-70-67(71)59-56-53-50-47-45-42-40-25-23-20-17-14-11-8-5-2/h16,19-20,23-24,26,28-29,31-32,34-35,66H,4-15,17-18,21-22,25,27,30,33,36-65H2,1-3H3,(H,70,71)(H,74,75)/p-1/b19-16-,23-20-,26-24-,29-28-,32-31-,35-34-/t66-/m1/s1",  # noqa: E501
        ),
    ],
)
def test_dunder_eq(inchikey: str, inchi: str):
    """Test that two identical structure identifiers are equal."""
    assert StructureIdentifier(inchikey=inchikey, inchi=inchi) == StructureIdentifier(
        inchikey=inchikey, inchi=inchi
    )


@pytest.mark.parametrize(
    "inchikey, inchi, expected_key, expected",
    [
        (
            "UNXRWKVEANCORM-UHFFFAOYSA-N",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
            "UNXRWKVEANCORM-UHFFFAOYSA-N",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
        ),
        (
            "UNXRWKVEANCORM-UHFFFAOYSA-I",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p-5",
            "UNXRWKVEANCORM-UHFFFAOYSA-N",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
        ),
        (
            "UNXRWKVEANCORM-UHFFFAOYSA-J",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p-5/b",
            "UNXRWKVEANCORM-UHFFFAOYSA-N",
            "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
            "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/b",
        ),
    ],
)
def test_neutralize_protonation(
    inchikey: str, inchi: str, expected_key: str, expected: str
):
    """Test that protonation state is neutralized correctly."""
    identifier = StructureIdentifier(
        inchikey=inchikey, inchi=inchi
    ).neutralize_protonation()
    assert identifier.inchikey == expected_key
    assert identifier.inchi == expected
