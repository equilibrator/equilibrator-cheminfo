# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Assert that InChI helper functions produce expected outcomes."""


import pandas as pd

from equilibrator_cheminfo.domain.model import Structure, StructureIdentifier


def test_neutralize_protonation():
    """Test that a structures table neutralizes protonation and is deduplicated."""
    df = pd.DataFrame(
        {
            "inchikey": [
                "UNXRWKVEANCORM-UHFFFAOYSA-N",
                "UNXRWKVEANCORM-UHFFFAOYSA-J",
                "UNXRWKVEANCORM-UHFFFAOYSA-O",
            ],
            "inchi": [
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p-5",
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p+1",
            ],
            "smiles": None,
        }
    )
    expected = pd.DataFrame(
        {
            "inchikey": [
                "UNXRWKVEANCORM-UHFFFAOYSA-N",
            ],
            "inchi": [
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
            ],
            "smiles": None,
        }
    )
    pd.testing.assert_frame_equal(df.cheminfo.neutralize_protonation(), expected)


def test_iter_structures():
    """Test that we indeed get an iterator over structures in order from a table."""
    df = pd.DataFrame(
        {
            "inchikey": [
                "UNXRWKVEANCORM-UHFFFAOYSA-N",
                "UNXRWKVEANCORM-UHFFFAOYSA-J",
                "UNXRWKVEANCORM-UHFFFAOYSA-O",
            ],
            "inchi": [
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p-5",
                "InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p+1",
            ],
            "smiles": None,
        }
    )
    expected = [
        Structure(
            identifier=StructureIdentifier(
                inchikey="UNXRWKVEANCORM-UHFFFAOYSA-N",
                inchi="InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)",
            )
        ),
        Structure(
            identifier=StructureIdentifier(
                inchikey="UNXRWKVEANCORM-UHFFFAOYSA-J",
                inchi="InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p-5",
            )
        ),
        Structure(
            identifier=StructureIdentifier(
                inchikey="UNXRWKVEANCORM-UHFFFAOYSA-O",
                inchi="InChI=1S/H5O10P3/c1-11(2,3)9-13(7,8)10-12(4,5)6/"
                "h(H,7,8)(H2,1,2,3)(H2,4,5,6)/p+1",
            )
        ),
    ]
    assert list(df.cheminfo.iter_structures()) == expected
