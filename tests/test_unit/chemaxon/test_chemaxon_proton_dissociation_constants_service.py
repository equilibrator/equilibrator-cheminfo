# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Test that the pKa ChemAxon adapter yields expected results."""


import os
from typing import List

import numpy as np
import pytest


if "CHEMAXON_HOME" not in os.environ:
    pytest.skip("CHEMAXON_HOME is not defined", allow_module_level=True)

from equilibrator_cheminfo.infrastructure.service.chemaxon import (
    ChemAxonMolecule,
    ChemAxonProtonDissociationConstantsService,
)


@pytest.fixture(scope="function")
def default_molecule() -> ChemAxonMolecule:
    """Return a default molecule."""
    return ChemAxonMolecule.from_smiles("NC(=O)CC(O)=O")


@pytest.fixture(scope="function")
def default_service() -> ChemAxonProtonDissociationConstantsService:
    """Return a default service."""
    return ChemAxonProtonDissociationConstantsService()


@pytest.mark.parametrize(
    "attributes",
    [
        {},
        {
            "minimum_ph": 2.0,
            "maximum_ph": 9.0,
            "ph_step": 0.5,
            "minimum_basic_pka": -5.0,
            "maximum_acidic_pka": 5.0,
            "use_large_model": True,
        },
        pytest.param(
            {"minimum_ph": 8.0, "maximum_ph": 5.0},
            marks=pytest.mark.raises(exception=AssertionError),
        ),
        pytest.param(
            {"ph_step": 0.0},
            marks=pytest.mark.raises(exception=AssertionError),
        ),
        pytest.param(
            {"minimum_basic_pka": 8.0, "maximum_acidic_pka": 2.0},
            marks=pytest.mark.raises(exception=AssertionError),
        ),
    ],
)
def test_init(attributes: dict):
    """Test that initialization parameters can be set."""
    assert isinstance(
        ChemAxonProtonDissociationConstantsService(**attributes),
        ChemAxonProtonDissociationConstantsService,
    )


def test_set_molecule(
    default_service: ChemAxonProtonDissociationConstantsService,
    default_molecule: ChemAxonMolecule,
):
    """Test that a molecule can be set on the adapter."""
    default_service.molecule = default_molecule
    assert default_service.molecule is default_molecule


def test_get_ph_values(default_molecule: ChemAxonMolecule):
    """Test that the pH range is set correctly."""
    service = ChemAxonProtonDissociationConstantsService(
        minimum_ph=2.0, maximum_ph=8.0, ph_step=2.0
    )
    assert (service.get_ph_values(default_molecule) == np.linspace(2.0, 8.0, 4)).all()


def test_get_dissociation_constants_within_ph_range(default_molecule: ChemAxonMolecule):
    """Test that pKa values returned are within set bounds that correspond to pH."""
    service = ChemAxonProtonDissociationConstantsService(
        minimum_ph=2.0,
        maximum_ph=9.0,
        minimum_basic_pka=2.0,
        maximum_acidic_pka=9.0,
    )
    assert service.estimate_pka_values(default_molecule) == pytest.approx(
        [3.66], abs=0.01
    )


def test_get_dissociation_constants_broader_pka_range(
    default_molecule: ChemAxonMolecule,
):
    """Test that pKa values returned are outside of pH bounds."""
    service = ChemAxonProtonDissociationConstantsService(
        minimum_ph=0.0,
        maximum_ph=14.0,
        minimum_basic_pka=-10.0,
        maximum_acidic_pka=20.0,
    )
    assert service.estimate_pka_values(default_molecule) == pytest.approx(
        [15.88, 3.66, -5.85], abs=0.01
    )


@pytest.mark.parametrize(
    "inchi, expected",
    [
        # adenine
        (
            "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
            [19.61, 9.84, 2.51, -1.95, -6.65],
        ),
        # acetate
        ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", [4.54]),
        # benzene
        ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", []),
        # 3-benzamido-4-hydroxybenzoic acid (3B4HA)
        (
            "InChI=1S/C14H11NO4/c16-12-7-6-10(14(18)19)8-11(12)15-13(17)9-4-2-1-3-5-9/"
            "h1-8,16H,(H,15,17)(H,18,19)",
            [9.54, 5.49, 4.62, -0.74, -6.43],
        ),
    ],
)
def test_get_dissociation_constants_known(inchi: str, expected: List[float]):
    """Confirm pKa values for some known structures."""
    service = ChemAxonProtonDissociationConstantsService(
        minimum_ph=0.0,
        maximum_ph=14.0,
        minimum_basic_pka=-10.0,
        maximum_acidic_pka=20.0,
    )
    assert service.estimate_pka_values(
        ChemAxonMolecule.from_inchi(inchi)
    ) == pytest.approx(expected, abs=0.01)


@pytest.mark.parametrize(
    "smiles, expected",
    [
        # 3-benzamido-4-hydroxybenzoic acid (3B4HA)
        (
            "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
            [15.99, 8.92, 4.23, -4.02, -6.29],
        ),
    ],
)
def test_get_dissociation_constants_from_smiles(smiles: str, expected: List[float]):
    """Confirm pKa values for some known structures."""
    service = ChemAxonProtonDissociationConstantsService(
        minimum_ph=0.0,
        maximum_ph=14.0,
        minimum_basic_pka=-10.0,
        maximum_acidic_pka=20.0,
    )
    assert service.estimate_pka_values(
        ChemAxonMolecule.from_smiles(smiles)
    ) == pytest.approx(expected, abs=0.01)
