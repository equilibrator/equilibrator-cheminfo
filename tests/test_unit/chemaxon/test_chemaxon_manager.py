# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Assert that the ChemAxon manager behaves as expected."""


import os

import jpype
import pytest


if "CHEMAXON_HOME" not in os.environ:
    pytest.skip("CHEMAXON_HOME is not defined", allow_module_level=True)

from equilibrator_cheminfo.infrastructure.service.chemaxon.chemaxon_manager import (
    ChemAxonManager,
)


def test_init_is_disabled():
    """Assert that the class cannot be called."""
    with pytest.raises(RuntimeError):
        ChemAxonManager()


def test_get_instance():
    """Assert that the ``get_instance`` method indeed returns an instance."""
    assert isinstance(ChemAxonManager.get_instance(), ChemAxonManager)


def test_chemaxon_package():
    """Assert that the chemaxon property returns a Java package instance."""
    assert isinstance(ChemAxonManager.get_instance().chemaxon, jpype.JPackage)
