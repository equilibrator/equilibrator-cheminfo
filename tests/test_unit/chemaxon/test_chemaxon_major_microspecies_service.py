# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Test that the major microspecies ChemAxon adapter yields expected results."""


import os

import pytest


if "CHEMAXON_HOME" not in os.environ:
    pytest.skip("CHEMAXON_HOME is not defined", allow_module_level=True)

from equilibrator_cheminfo.infrastructure.service.chemaxon import (
    ChemAxonMajorMicrospeciesService,
    ChemAxonMolecule,
)


@pytest.fixture(scope="function")
def default_molecule() -> ChemAxonMolecule:
    """Return a default molecule for unit tests."""
    return ChemAxonMolecule.from_smiles("NC(=O)CC(O)=O")


@pytest.fixture(scope="function")
def default_service() -> ChemAxonMajorMicrospeciesService:
    """Return a default service."""
    return ChemAxonMajorMicrospeciesService()


@pytest.mark.parametrize(
    "attributes",
    [
        {},
        {"ph": 2.0},
        {"keep_hydrogens": True},
    ],
)
def test_init(attributes: dict):
    """Test that initialization parameters can be set."""
    assert isinstance(
        ChemAxonMajorMicrospeciesService(**attributes), ChemAxonMajorMicrospeciesService
    )


def test_set_molecule(
    default_service: ChemAxonMajorMicrospeciesService,
    default_molecule: ChemAxonMolecule,
):
    """Test that a molecule can be set on the adapter."""
    default_service.molecule = default_molecule
    assert default_service.molecule is default_molecule


@pytest.mark.parametrize(
    "inchi, expected",
    [
        (
            "InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p-1",
            "[O-]C(=[NH2+])CC([O-])=O",
        ),
        ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", "CC([O-])=O"),
        ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", "C1=CC=CC=C1"),
    ],
)
def test_get_major_microspecies_from_inchi(default_service, inchi: str, expected: str):
    """Test that the major microspecies at pH 7 has the expected SMILES."""
    assert (
        default_service.estimate_major_microspecies(
            ChemAxonMolecule.from_inchi(inchi)
        ).smiles
        == expected
    )
