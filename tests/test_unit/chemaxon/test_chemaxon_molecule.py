# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Test that the ChemAxon molecule correctly conforms with the abstract interface."""


import importlib.util
import os
from pathlib import Path

import pytest


if "CHEMAXON_HOME" not in os.environ:
    pytest.skip("CHEMAXON_HOME is not defined", allow_module_level=True)

from equilibrator_cheminfo.infrastructure.chemaxon.domain import ChemAxonMolecule


helpers = Path(__file__).resolve().parent.parent / "helpers" / "check_molecule.py"
spec = importlib.util.spec_from_file_location("check_molecule", helpers)
check_molecule = importlib.util.module_from_spec(spec)
spec.loader.exec_module(check_molecule)


class TestChemAxonMolecule(check_molecule.TestMolecule):
    """Instantiate the abstract tests for the ChemAxon molecule."""

    klass = ChemAxonMolecule
