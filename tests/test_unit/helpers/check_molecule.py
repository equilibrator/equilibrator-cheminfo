# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Provide a class with unit tests for the abstract molecule interface."""


from typing import Dict, Tuple

import pytest

from equilibrator_cheminfo.domain.model import (
    AbstractMolecule,
    EquilibratorCheminformaticsError,
)


class TestMolecule:
    """
    Define the unit tests for the molecule interface.

    The test cases should be made concrete by setting the respective molecule class on
    the class attribute.

    """

    klass = AbstractMolecule

    def test_from_inchi(self, inchi: str):
        """Test initialization from InChI."""
        assert isinstance(self.klass.from_inchi(inchi), self.klass)

    @pytest.mark.parametrize(
        "inchi, expected",
        [
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p-2", 10),
            ("InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)", 15),
            ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", 7),
            ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", 12),
        ],
    )
    def test_from_inchi_dunder_len(self, inchi: str, expected: int):
        """Test that a molecule reports the expected number of atoms."""
        assert len(self.klass.from_inchi(inchi)) == expected

    def test_get_inchi_from_inchi(self, inchi: str):
        """Test that an InChI can be round-tripped."""
        assert self.klass.from_inchi(inchi).inchi == inchi

    def test_get_inchikey_from_inchi(self, inchikey_pair: Tuple[str, str]):
        """Test that the expected InChIKey is generated from an InChI."""
        inchi, expected = inchikey_pair
        assert self.klass.from_inchi(inchi).inchikey == expected

    def test_get_smiles_from_inchi(self, inchi_smiles_pair: Tuple[str, str]):
        """Test that an equivalent SMILES is generated from an InChI."""
        inchi, smiles = inchi_smiles_pair
        assert self.klass.from_smiles(
            self.klass.from_inchi(inchi).smiles
        ) == self.klass.from_smiles(smiles)

    @pytest.mark.parametrize(
        "inchi, expected",
        [
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p-2", "C3H3NO3-2"),
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p+1", "C3H6NO3+"),
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p+2", "C3H7NO3+2"),
            # adenine
            (
                "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
                "C5H5N5",
            ),
            # acetate
            ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", "C2H3O2-"),
            # benzene
            ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", "C6H6"),
        ],
    )
    def test_get_molecular_formula_from_inchi(self, inchi: str, expected: str):
        """Test that the expected molecular formula is generated from an InChI."""
        assert self.klass.from_inchi(inchi).molecular_formula == expected

    @pytest.mark.parametrize(
        "inchi, expected",
        [
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p-1", 102.07),
            # adenine
            ("InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)", 135.1),
            # acetate
            ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", 59.0),
            # benzene
            ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", 78.1),
        ],
    )
    def test_get_molecular_mass_from_inchi(self, inchi: str, expected: float):
        """Test that a molecule generated from an InChI has expected mass."""
        assert self.klass.from_inchi(inchi).molecular_mass == pytest.approx(
            expected, abs=0.1
        )

    @pytest.mark.parametrize(
        "inchi, expected",
        [
            ("InChI=1S/C3H5NO3/c4-2(5)1-3(6)7/h1H2,(H2,4,5)(H,6,7)/p-1", -1),
            ("InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1", -1),
            ("InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H", 0),
        ],
    )
    def test_get_charge_from_inchi(self, inchi: str, expected: int):
        """Test that a molecule generated from an InChI has expected charge."""
        assert self.klass.from_inchi(inchi).charge == expected

    @pytest.mark.parametrize(
        "smiles, expected",
        [
            pytest.param(
                "",
                {},
                marks=pytest.mark.raises(exception=EquilibratorCheminformaticsError),
            ),
            ("CC=O", {"C": 2, "O": 1, "H": 4, "e-": 24}),
            ("C#N", {"C": 1, "H": 1, "N": 1, "e-": 14}),
            ("NC(=O)CC(O)=O", {"N": 1, "C": 3, "O": 3, "H": 5, "e-": 54}),
            (
                "[*][*][Fe++]1([*][*])[S--][Fe+3]([*][*])([*][*])[S--]1",
                {"Fe": 2, "S": 2, "e-": 83},
            ),
        ],
    )
    def test_get_atom_bag(self, smiles: str, expected: Dict[str, int]):
        """Test that a molecule generated from an InChI has an expected atom bag."""
        assert self.klass.from_smiles(smiles).atom_bag == expected
