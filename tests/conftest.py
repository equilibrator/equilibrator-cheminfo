# Copyright (c) 2021, Moritz E. Beber.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Provide fixtures to tests in the directory hierarchy."""


from pathlib import Path
from typing import Tuple

import pandas as pd
import pytest


deduplicated_inchis = pd.read_csv(
    Path(__file__).parent / "data/deduplicated.tsv", sep="\t"
).iloc[:5, :]


raw_inchis = pd.read_csv(Path(__file__).parent / "data/raw_inchis.tsv", sep="\t").iloc[
    :5, :
]


@pytest.fixture(scope="function", params=deduplicated_inchis["inchikey"])
def inchikey(request) -> str:
    """Return InChIKeys from a table in a parametrized fashion."""
    return request.param


@pytest.fixture(scope="function", params=deduplicated_inchis["inchi"])
def inchi(request) -> str:
    """Return InChIs from a table in a parametrized fashion."""
    return request.param


@pytest.fixture(scope="function", params=deduplicated_inchis["smiles"])
def smiles(request) -> str:
    """Return SMILES from a table in a parametrized fashion."""
    return request.param


@pytest.fixture(
    scope="function",
    params=list(
        deduplicated_inchis[["inchi", "inchi"]].itertuples(index=False, name=None)
    ),
)
def inchi_pair(request) -> Tuple[str, str]:
    """Return a pair of InChIs from a table in a parametrized fashion."""
    return request.param


@pytest.fixture(
    scope="function",
    params=list(
        deduplicated_inchis[["inchi", "inchikey"]].itertuples(index=False, name=None)
    ),
)
def inchikey_pair(request) -> Tuple[str, str]:
    """Return a pair of InChI, InChIKeys from a table in a parametrized fashion."""
    return request.param


@pytest.fixture(
    scope="function",
    params=list(raw_inchis[["inchi", "smiles"]].itertuples(index=False, name=None)),
)
def inchi_smiles_pair(request) -> Tuple[str, str]:
    """Return a pair of InChI, SMILES from a table in a parametrized fashion."""
    return request.param
